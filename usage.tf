# -----------------------------------------------------------------
# Example Terraform Configuration
# Shows how to use the Terraform modules
# -----------------------------------------------------------------
provider "aws" {
  region  = "us-east-1"
  version = ">= 2.47.0"
}

provider "random" {
  version = ">= 2.2.0"
}

variable "app_name" {
  default = "common-terraform-modules"
}

# -----------------------------------------------------------------
# Module imports
# -----------------------------------------------------------------
# Generate a random master password for the RDS database
resource "random_password" "db_password" {
  length           = 16
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?" # no @ for RDS
}

data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = ["vpc"]
  }
}

data "aws_subnet_ids" "subnets" {
  vpc_id = data.aws_vpc.vpc.id

  filter {
    name   = "tag:Name"
    values = ["*private*"]
  }
}

data "aws_kms_key" "kms_key" {
  key_id = "alias/aws/ssm" # Or a key identifier
}

module "rds-aurora-serverless" {
  source                        = "./rds-aurora-serverless"
  database_name                 = replace(var.app_name, "-", "_")
  env_prefix                    = "${var.app_name}-test"
  master_db_user                = replace(var.app_name, "-", "_")
  master_db_password            = random_password.db_password.result
  subnet_ids                    = tolist(data.aws_subnet_ids.subnets.ids)
  kms_key_id                    = data.aws_kms_key.kms_key.key_id
  allow_client_outbound_traffic = true
}