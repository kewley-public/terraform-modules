# Terraform Modules

Within this repo you will find some of the common modules that I use throughout my applications

## Module Structure

These modules should follow the [Terraform recommended structure](https://www.terraform.io/docs/modules/create.html)

Each subfolder will have at least three files: `main.tf`, `variables.tf`, and `outputs.tf`

```
$ tree minimal-module/
.
|-- main.tf
|-- variables.tf
|-- outputs.tf
```

### RDS-Aurora-Serverless

This module creates a new serverless database cluster with RDS Aurora, a new Parameter store paramter with the database 
credentials, and a pair of client/server Security Groups for controllin access.

Variables:
* `allow_client_outbound_traffic` (Optional) - Whether to allow other outbound traffic for the client security group (Defaults to false)
* `auto_pause` (Optional) - Whether to enable automatic pause of the database when idle (Defaults to true)
* `data_api` (Optional) - Whether to enable HTTP endpoint for Aurora Serverless Data API (Defaults to false)
* `database_name` - Name for the database
* `engine` (Optional) - Database engine to use (Defaults to `aurora-postgresql`, also supports `aurora-mysql`)
* `env_prefix` - Prefix used to namespace resources
* `master_db_user` - Master username for the database
* `master_db_password` - Master password for the database
* `max_capacity` (Optional) - Maximum number of Aurora capacity units (ACUs) to use for the database (Defaults to 2)
* `min_capacity` (Optional) - Minimum number of Aurora capacity units (ACUs) to use for the database (Defaults to 2)
* `seconds_until_auto_pause` (Optional) - The time, in seconds, before the database will be paused when idle (Defaults to 3600)
* `security_group_ids` (Optional) - List of additional VPC Security Group Ids needing access to the database
* `subnet_ids` - List of VPC Subnet Ids to use for database network interfaces
* `kms_key_id` (Optional) - The kms key identifier (alias is ok too) used for encrypting the database config SSM parameter (Defaults to null which is aws kms)

Outputs:

* `rds_client_security_group_id` - Security Group Id for clients to access RDS Aurora Cluster
* `rds_cluster_arn` - ARN for the Aurora Cluster
* `rds_cluster_database_name` - Name of the database
* `rds_cluster_endpoint` - Endpoint to use when connecting to the Aurora Cluster
* `rds_cluster_maintenance_window` - Maintenance window for the Aurora Cluster
* `rds_cluster_master_username` - Username to use when connecting to the Aurora Cluster
* `rds_cluster_port` - Port to use when connecting to the Aurora Cluster
* `parameter_group_name` - The parameter name that holds the Aurora Cluster configuration
* `parameter_group_kms_key_id` - The KMS key id used for encrypting the Aurora Cluster configuration

**IMPORTANT:** RDS has certain naming constraints for some of these variables: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_Limits.html#RDS_Limits.Constraints

**NOTE:** Changes to a running database are not applied immediately, and instead are applied during the next available maintenance window: https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Overview.DBInstance.Modifying.html

**NOTE:** Termination protection is enabled on the Aurora cluster with this module. If the cluster needs to be deleted you will have to disable the protection through the AWS console first.

**NOTE:** Aurora Serverless limitations: https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/aurora-serverless.html#aurora-serverless.limitations

**NOTE:** As of 6/1/2020 Aurora Serverless now supports a min_capacity of 1 for MySQL, but still requires a min_capacity of 2 for Postgres

### Sample Usage with python
Note you will need to setup a VPC endpoint to communicate with SSM as communicating with the RDS client
is done within a VPC. See more [here](https://docs.aws.amazon.com/systems-manager/latest/userguide/setup-create-vpc.html)

```python
from cachetools import func
import logging
import sys
import os
import boto3
import json

# ------------------------------------------------------------------------
# Environment Variables for SSM parameter keys
# ------------------------------------------------------------------------
def fetch_ssm_parameter_group_base():
    return os.environ.get('SSM_BASE', 'parameterBaseName')

# This is the name created from terraform
def fetch_database_config_ssm_parameter():
    return os.environ.get('DATABASE_CONFIG', 'auroraServerlessConfig')


logger = None
def get_logger():
    global logger

    if logger is not None:
        return logger
    else:
        logger = logging.getLogger('My Cool Logger')
        logger.setLevel(logging.INFO)
        logger.propagate = False

        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        logger.addHandler(handler)

        return logger

# Helper class for fetching parameters
class SSM:
    def __init__(self):
        self.ssm = boto3.client('ssm')
        self.logger = get_logger()

    @func.ttl_cache(maxsize=100, ttl=300)
    def fetch_parameters(self) -> dict:
        parameters = {}
        try:
            # Get all parameters for this app
            self.logger.info('Fetching SSM parameters...')
            param_details = self.ssm.get_parameters_by_path(
                Path=f'/{fetch_ssm_parameter_group_base()}',
                Recursive=False,
                WithDecryption=True
            )
            self.logger.info('Parameters fetched!')

            # Loop through the returned parameters and populate the ConfigParser
            if 'Parameters' in param_details and len(param_details.get('Parameters')) > 0:
                for param in param_details.get('Parameters'):
                    param_path_array = param.get('Name').split('/')
                    section_position = len(param_path_array) - 1
                    section_name = param_path_array[section_position]
                    self.logger.info(f'Found configuration: {section_name}')
                    parameters[section_name] = param.get('Value')
        except Exception as e:
            self.logger.error(e)
        return parameters

@func.ttl_cache(maxsize=100, ttl=300)
def database_url():
    """Use DATABASE environment variables to provide a database connection URL."""
    log = get_logger()
    ssm = SSM()
    config = ssm.fetch_parameters().get(fetch_database_config_ssm_parameter())
    if config:
        log.info(f'Config fetched...fetching DB URL..')
        try:
            database_config = json.loads(config)
            scheme = 'postgresql' if database_config.get('engine') == 'aurora-postgresql' else 'mysql'
            user = database_config['username']
            password = database_config['password']
            host = database_config['host']
            port = database_config['port']
            db = database_config['name']
            return f'{scheme}://{user}:{password}@{host}:{port}/{db}?sslmode=require'
        except Exception as e:
            log.info(f'Failed to generate database_url from secret: {e}')
            raise e
    log.info(f'Config was empty or not needed...Returning default DB URL')
    return 'postgresql://postgres:postgres@127.0.0.1/postgres'
```
