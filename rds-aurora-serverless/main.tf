# ---------------------------------------------------------------------------
# Create serverless Aurora Cluster
# ---------------------------------------------------------------------------
resource "aws_db_subnet_group" "rds_aurora_serverless" {
  name       = "${var.env_prefix}-aurora-serverless"
  subnet_ids = var.subnet_ids

  tags = {
    Environment = var.env_prefix
  }
}

resource "aws_rds_cluster" "rds_aurora_serverless" {
  cluster_identifier     = "${var.env_prefix}-aurora-serverless"
  copy_tags_to_snapshot  = true
  enable_http_endpoint   = var.data_api
  engine                 = var.engine
  engine_mode            = "serverless"
  database_name          = var.database_name
  master_username        = var.master_db_user
  master_password        = var.master_db_password
  deletion_protection    = true
  vpc_security_group_ids = [aws_security_group.rds_server.id]
  db_subnet_group_name   = aws_db_subnet_group.rds_aurora_serverless.name

  scaling_configuration {
    auto_pause               = var.auto_pause
    max_capacity             = var.max_capacity
    min_capacity             = var.min_capacity
    seconds_until_auto_pause = var.seconds_until_auto_pause
  }

  tags = {
    Environment = var.env_prefix
  }
}

# ---------------------------------------------------------------------------
# Create Security Groups for accessing Aurora Cluster
# ---------------------------------------------------------------------------
data "aws_subnet" "first" {
  id = var.subnet_ids[0]
}

resource "aws_security_group" "rds_server" {
  name        = "${var.env_prefix}-rds-server"
  description = "Controls access to RDS Aurora Cluster"
  vpc_id      = data.aws_subnet.first.vpc_id

  ingress {
    from_port       = var.engine == "aurora-postgresql" ? 5432 : 3306
    to_port         = var.engine == "aurora-postgresql" ? 5432 : 3306
    protocol        = "tcp"
    security_groups = concat([aws_security_group.rds_client.id], var.security_group_ids)
  }

  tags = {
    Environment = var.env_prefix
  }
}

resource "aws_security_group" "rds_client" {
  name        = "${var.env_prefix}-rds-client"
  description = "Allows client instances to access RDS Aurora Cluster"
  vpc_id      = data.aws_subnet.first.vpc_id

  # Needed to allow for VPC endpoints to work
  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    description = "Allow all traffic in from self"
    self        = true
  }

  tags = {
    Environment = var.env_prefix
  }
}

resource "aws_security_group_rule" "rds_client_limited_egress" {
  # always allow limited egress for rds client
  type                     = "egress"
  from_port                = var.engine == "aurora-postgresql" ? 5432 : 3306
  to_port                  = var.engine == "aurora-postgresql" ? 5432 : 3306
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.rds_server.id
  security_group_id        = aws_security_group.rds_client.id
}

resource "aws_security_group_rule" "rds_client_full_egress" {
  # allow all egress for rds clients if allow_client_outbound_traffic is true
  count = var.allow_client_outbound_traffic == true ? 1 : 0

  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = aws_security_group.rds_client.id
}

# ---------------------------------------------------------------------------
# Create Parameter Store secret with database credentials
# ---------------------------------------------------------------------------
resource "aws_ssm_parameter" "rds_aurora_serverless" {
  name        = "/${var.env_prefix}/auroraServerlessConfig"
  description = "The Aurora RDS serverless configuration"
  type        = "SecureString"
  key_id      = var.kms_key_id
  overwrite   = true
  value = jsonencode({
    dbClusterIdentifier = aws_rds_cluster.rds_aurora_serverless.cluster_identifier
    name                = aws_rds_cluster.rds_aurora_serverless.database_name
    engine              = aws_rds_cluster.rds_aurora_serverless.engine
    host                = aws_rds_cluster.rds_aurora_serverless.endpoint
    port                = aws_rds_cluster.rds_aurora_serverless.port
    username            = aws_rds_cluster.rds_aurora_serverless.master_username
    password            = aws_rds_cluster.rds_aurora_serverless.master_password
  })

  tags = {
    Environment = var.env_prefix
  }
}