# ---------------------------------------------------------------------
# REQUIRED PARAMETERS
# You must provide a value for each of these parameters
# ---------------------------------------------------------------------
variable "database_name" {
  type        = string
  description = "Name for the database"
}

variable "env_prefix" {
  type        = string
  description = "Prefix used to namespace resources"
}

variable "master_db_user" {
  type        = string
  description = "Master username for the database"
}

variable "master_db_password" {
  type        = string
  description = "Master password for the database"
}

variable "subnet_ids" {
  type        = list(string)
  description = "List of VPC Subnet Ids to use for database network interfaces"
}
# ---------------------------------------------------------------------
# OPTIONAL PARAMETERS
# ---------------------------------------------------------------------
variable "kms_key_id" {
  type        = string
  description = "The kms key for encrypting the database config (Defaults to null which uses aws kms)"
  default     = null
}

variable "allow_client_outbound_traffic" {
  type        = bool
  default     = false
  description = "Whether to allow other outbound traffic for the client security group (Defaults to false)"
}

variable "auto_pause" {
  type        = bool
  default     = true
  description = "Whether to enable automatic pause of the database when idle (Defaults to true)"
}

variable "data_api" {
  type        = bool
  default     = false
  description = "Whether to enable HTTP endpoint for Aurora Serverless Data API (Defaults to false)"
}

variable "engine" {
  type        = string
  default     = "aurora-postgresql"
  description = "Database engine to use (Defaults to `aurora-postgresql`, also supports `aurora-mysql`)"
}

variable "max_capacity" {
  type        = number
  default     = 2
  description = "Maximum number of Aurora capacity units (ACUs) to use for the database (Defaults to 2)"
}

variable "min_capacity" {
  type        = number
  default     = 2
  description = "Minimum number of Aurora capacity units (ACUs) to use for the database (Defaults to 2)"
}

variable "seconds_until_auto_pause" {
  type        = number
  default     = 3600
  description = "The time, in seconds, before the database will be paused when idle (Defaults to 3600)"
}

variable "security_group_ids" {
  type        = list(string)
  default     = []
  description = "List of VPC Security Group Ids for controlling access to the database"
}